using System.ServiceModel;
using System.Threading.Tasks;
using MyServiceBus.Grpc.Contracts;

namespace MyServiceBus.Grpc
{

    [ServiceContract(Name = "Subscriber")]
    public interface ISubscriberGrpcService
    {
        [OperationContract(Action = "GetMessages")]
        ValueTask<SubscribeGrpcResponse> GetMessagesAsync(SubscribeGrpcRequest request);
    }
}