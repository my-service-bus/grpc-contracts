using System.Collections.Generic;
using System.Runtime.Serialization;
using MyServiceBus.Grpc.Models;

namespace MyServiceBus.Grpc.Contracts
{
    [DataContract]
    public class PublishMessageGrpcRequest
    {
        [DataMember(Order = 1)]
        public long SessionId { get; set; }
        
        [DataMember(Order = 2)]
        public string TopicId { get; set; }

        [DataMember(Order = 3)]
        public bool PersistImmediately { get; set; }
        
        [DataMember(Order = 4)]
        public IEnumerable<byte[]> Messages { get; set; }
    }

    [DataContract]
    public class PublishMessageGrpcResponse
    {
        [DataMember(Order = 1)]
        public GrpcResponseStatus Status { get; set; }
    }
    
}