using System.Collections.Generic;
using System.Runtime.Serialization;
using MyServiceBus.Grpc.Models;

namespace MyServiceBus.Grpc.Contracts
{
    [DataContract]
    public class SubscribeGrpcRequest
    {
        [DataMember(Order = 1)]
        public long SessionId { get; set; }
        
        [DataMember(Order = 2)]
        public string TopicId { get; set; }

        [DataMember(Order = 3)]
        public string QueueId { get; set; }
        
        [DataMember(Order = 4)]
        public long ConfirmationId { get; set; }
    }
    
    [DataContract]
    public class SubscribeGrpcResponse
    {
        [DataMember(Order = 1)]
        public GrpcResponseStatus Status { get; set; }
        
        [DataMember(Order = 2)]
        public long ConfirmationId { get; set; }
        
        [DataMember(Order = 3)]
        public IEnumerable<MessageGrpcModel> NewMessages { get; set; }
    }
}