using System.Runtime.Serialization;
using MyServiceBus.Grpc.Models;

namespace MyServiceBus.Grpc.Contracts
{

    [DataContract]
    public class CreateTopicGrpcRequest
    {
        [DataMember(Order = 1)]
        public long SessionId { get; set; }
        [DataMember(Order = 2)]
        public string TopicId { get; set; }
        [DataMember(Order = 3)]
        public int MaxCachedMessagesCount { get; set; }
    }


    [DataContract]
    public class CreateTopicGrpcResponse
    {
        [DataMember(Order = 1)] 
        public GrpcResponseStatus Status { get; set; }
        [DataMember(Order = 2)]
        public long SessionId { get; set; }
    }

}