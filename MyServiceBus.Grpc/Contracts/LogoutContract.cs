using System.Runtime.Serialization;

namespace MyServiceBus.Grpc.Contracts
{
    [DataContract]
    public class LogoutGrpcRequest
    {
        [DataMember(Order = 1)]
        public long SessionId { get; set; }
    }
    
    [DataContract]
    public class LogoutGrpcResponse
    {
        [DataMember(Order = 1)]
        public long SessionId { get; set; }
    }
}