using System.Runtime.Serialization;
using MyServiceBus.Grpc.Models;

namespace MyServiceBus.Grpc.Contracts
{

    public enum QueueTypeGrpcEnum
    {
        Permanent, AutoDelete, PermanentWithSingleConnect
    }
    
    
    [DataContract]
    public class CreateQueueGrpcRequest
    {
        [DataMember(Order = 1)]
        public long SessionId { get; set; }
        
        [DataMember(Order = 2)]
        public string TopicId { get; set; }
        
        [DataMember(Order = 3)]
        public string QueueId { get; set; }
        
        [DataMember(Order = 4)]
        public QueueTypeGrpcEnum QueueType { get; set; } 
    }

    [DataContract]
    public class CreateQueueGrpcResponse
    {
        [DataMember(Order = 1)] 
        public GrpcResponseStatus Status { get; set; }
        
        [DataMember(Order = 2)] 
        public long SessionId { get; set; }
    }
}