using System.Runtime.Serialization;
using MyServiceBus.Grpc.Models;

namespace MyServiceBus.Grpc.Contracts
{
    [DataContract]
    public class GreetingGrpcRequest
    {
        [DataMember(Order = 1)]
        public string Name { get; set; }
    }

    [DataContract]
    public class GreetingGrpcResponse
    {
        [DataMember(Order = 1)] 
        public GrpcResponseStatus Status { get; set; }
        [DataMember(Order = 2)]
        public long SessionId { get; set; }
    }

    [DataContract]
    public class PingGrpcRequest
    {
        [DataMember(Order = 1)]
        public long SessionId { get; set; }
    }

    
    [DataContract]
    public class PingGrpcResponse
    {
        [DataMember(Order = 1)] 
        public GrpcResponseStatus Status { get; set; }
        [DataMember(Order = 2)]
        public long SessionId { get; set; }
    }
    
    

}