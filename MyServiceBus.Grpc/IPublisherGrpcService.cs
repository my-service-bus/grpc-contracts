﻿using System.ServiceModel;
using System.Threading.Tasks;
using MyServiceBus.Grpc.Contracts;

namespace MyServiceBus.Grpc
{
    [ServiceContract(Name = "Publisher")]
    public interface IPublisherGrpcService
    {
        [OperationContract(Action = "PublishMessage")]
        ValueTask<PublishMessageGrpcResponse> PublishMessageAsync(PublishMessageGrpcRequest request);
    }

}