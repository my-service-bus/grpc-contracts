using System.ServiceModel;
using System.Threading.Tasks;
using MyServiceBus.Grpc.Contracts;

namespace MyServiceBus.Grpc
{
    [ServiceContract(Name = "Management")]
    public interface IManagementGrpcService
    {
        [OperationContract(Action = "CreateTopic")]
        ValueTask<CreateTopicGrpcResponse> CreateTopicAsync(CreateTopicGrpcRequest request);

        [OperationContract(Action = "CreateQueue")]
        ValueTask<CreateQueueGrpcResponse> CreateQueueAsync(CreateQueueGrpcRequest request);

        [OperationContract(Action = "Greeting")]
        ValueTask<GreetingGrpcResponse> GreetingAsync(GreetingGrpcRequest request);

        [OperationContract(Action = "Ping")]
        ValueTask<PingGrpcResponse> PingAsync(PingGrpcRequest request);
        
        [OperationContract(Action = "Logout")]
        ValueTask<LogoutGrpcResponse> LogoutAsync(LogoutGrpcRequest request);
    }
}
