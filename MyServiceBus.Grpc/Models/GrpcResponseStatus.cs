namespace MyServiceBus.Grpc.Models
{
    public enum GrpcResponseStatus
    {
        Ok, SessionExpired, TopicNotFound
    }
}