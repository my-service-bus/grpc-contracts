using System.Runtime.Serialization;

namespace MyServiceBus.Grpc.Models
{
    [DataContract]
    public class MessageGrpcModel
    {
        
        [DataMember(Order = 1)]
        public long MessageId { get; set; }

        [DataMember(Order = 2)]
        public byte[] Message { get; set; }
        

    }
}